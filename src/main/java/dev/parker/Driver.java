package dev.parker;

import dev.parker.daos.*;
import dev.parker.models.Account;
import dev.parker.models.Customer;
import dev.parker.models.Transaction;
import dev.parker.models.TransactionType;
import dev.parker.ui.BankAppUserInterface;
import dev.parker.ui.UserInterface;

import java.util.*;

public class Driver {

    static UserInterface ui;
    static String input;
    static String username;
    static String password;
    static Customer customer;
    static Transaction txn;
    static Scanner sc;

    static AccountDAO acctDAO;
    static CustomerDAO custDAO;
    static TransactionDAO txnDAO;

    static Map<String, Customer> customersByUsername;
    static Map<Integer, Account> accounts;
    static List<Integer> accountNums;
    static List<Account> listOfAccounts;
    static Collection<Account> accountList;

    public static void run() {
        ui = new BankAppUserInterface();
        sc = new Scanner(System.in);

        acctDAO = new AccountDAOImpl();
        custDAO = new CustomerDAOImpl();
        txnDAO = new TransactionDAOImpl();

        customersByUsername = custDAO.getAllCustomersByUsername();
        accounts = acctDAO.getAllAccountsADMIN();
        accountNums = new ArrayList<>();
        accountList = accounts.values();

        for (int i = 0; i < accountList.size(); i++) {
            accountNums.add(1);
        }

        input = ui.welcome();

        if (input.matches("1")) {
            System.out.println("Thank you for choosing JBW!...............................................");
            username = ui.newUserWelcome();
            while (customersByUsername.containsKey(username)) { // checks whether a valid username is available
                System.out.println("Sorry, that username is unavailable.......................................");
                username = ui.newUserWelcome();
            }
            System.out.println("Thank you! Your username is: " + username);
            password = ui.createPassword();
            customer = new Customer(username, password);
        // add this new customer to db
            custDAO.createNewCustomer(customer);
        // attach cust_id from db to customer instance
            customer.setId(custDAO.getCustomer(username).getId());

            input = ui.newUserMainMenu(customer);

            while (!input.matches("1") && !input.matches("2")) {
                System.out.println("Invalid Input.............................................................");
                input = ui.newUserMainMenu(customer);
            }
            if (input.matches("1")) {
                openAccount();
            } else if (input.matches("2")) {
                for (int i = 0; i < 1000; i++) {
                    System.out.println("");
                }
                System.exit(0);
            }
        } else if (customersByUsername.containsKey(input)) {
            username = input;
            customer = customersByUsername.get(username);
            String storedPass = customer.getPassword();
            System.out.println("Thank you " + username + "!");
            password = ui.enterPassword();
            while (!password.matches(storedPass)) {
                System.out.println("incorrect password........................................................");
                password = ui.enterPassword();
            }

            // attach relevant accounts to customer object
            for (Account a : accountList) {
                if (a.getOwnerId() == customer.getId()) {
                    customer.addAccount(a);
                }
            }
        } else {
            System.out.println("Something went wrong......................................................");
            run();
        }
        System.out.println("Welcome " + customer.getUsername() + "! ");
        input = ui.mainMenu(customer);
        while (input.matches("[1234567]")) {
//            input = ui.mainMenu(customer);
            if (input.matches("1")) { // show all information for each account
                listOfAccounts = customer.getAccounts();
                System.out.println("Your Accounts.............................................................");
                for (int i = 0; i < listOfAccounts.size(); i++) {
                    Account a = listOfAccounts.get(i);
                    System.out.println(i+1 +". " + a.getAccountType() + " #" + a.getAccountNumber() + " balance: $" + String.format("%.2f", a.getBalance()));
                }
            } else if (input.matches("2")) { // make a deposit
                listOfAccounts = customer.getAccounts();
                int n = 0;
                while (true) {
                    System.out.println("Select an account to make deposit.........................................");
                    System.out.println("..........................................................................");
                    System.out.println("Enter a number to select an option........................................");
                    for (int i = 0; i < listOfAccounts.size(); i++) {
                        Account a = listOfAccounts.get(i);
                        System.out.println(i+1 +". " + a.getAccountType() + " #" + a.getAccountNumber() + " balance: $" + String.format("%.2f", a.getBalance()));
                    }
                    try {
                        input = sc.nextLine();
                        n = Integer.parseInt(input);
                        Account acct = listOfAccounts.get(n-1);
                        System.out.println("depositing into accnt #" + acct.getAccountNumber());
                        double amt = ui.deposit(acct);
                        Transaction txn = new Transaction(TransactionType.DEPOSIT, amt);
                        txn.setFrom(0); // this is a deposit so no from account
                        txn.setTo(acct.getAccountNumber()); // to account

                        txnDAO.createTransaction(txn); // add txn to db
                        txn = txnDAO.getLastTransaction(); // identify just created record with this txn instance

                        //add funds to to account's balance
                        acctDAO.deposit(acct, txn);
                        acct.setBalance(acct.getBalance() + amt);
                        System.out.println("Successful deposit!.......................................................");
                        System.out.println(acct.getAccountType() + " #" + acct.getAccountNumber() + " new balance: $" + String.format("%.2f", acct.getBalance()));
                        Thread.sleep(2000);
                        break;

                    } catch (NumberFormatException e) {
                        System.out.println("***INVALID INPUT***.......................................................");
                    } catch (IndexOutOfBoundsException e) {
                        System.out.println("***INVALID INPUT***.......................................................");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } else if (input.matches("3")) { // make a withdrawal
                listOfAccounts = customer.getAccounts();
                int n = 0;
                while (true) {
                    System.out.println("Select an account to make withdraw........................................");
                    System.out.println("..........................................................................");
                    System.out.println("Enter a number to select an option........................................");
                    for (int i = 0; i < listOfAccounts.size(); i++) {
                        Account a = listOfAccounts.get(i);
                        System.out.println(i+1 +". " + a.getAccountType() + " #" + a.getAccountNumber() + " balance: $" + String.format("%.2f", a.getBalance()));
                    }
                    try {
                        input = sc.nextLine();
                        n = Integer.parseInt(input);
                        Account acct = listOfAccounts.get(n-1);
                        System.out.println("withdrawing from accnt #" + acct.getAccountNumber());
                        double amt = ui.withdraw(acct);
                        Transaction txn = new Transaction(TransactionType.WITHDRAWAL, amt);
                        txn.setTo(0); // this is a withdraw so no to account
                        txn.setFrom(acct.getAccountNumber()); // from account

                        txnDAO.createTransaction(txn); // add txn to db
                        txn = txnDAO.getLastTransaction(); // identify just created record with this txn instance

                        // remove funds from from account's balance
                        acctDAO.withdraw(acct, txn);
                        acct.setBalance(acct.getBalance() - amt);
                        System.out.println("Successful Withdraw!......................................................");
                        System.out.println(acct.getAccountType() + " #" + acct.getAccountNumber() + " new balance: $" + String.format("%.2f", acct.getBalance()));
                        Thread.sleep(2000);
                        break;

                    } catch (NumberFormatException e) {
                        System.out.println("***INVALID INPUT***.......................................................");
                    } catch (IndexOutOfBoundsException e) {
                        System.out.println("***INVALID INPUT***.......................................................");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } else if (input.matches("4")) { // open a new account
                openAccount();
            } else if (input.matches("5")) { // list of transfers

                listOfAccounts = customer.getAccounts();
                int n;
                while (true) {
                    System.out.println("Select an account to see its transaction history..........................");
                    System.out.println("..........................................................................");
                    System.out.println("Enter a number to select an option........................................");
                    for (int i = 0; i < listOfAccounts.size(); i++) {
                        Account a = listOfAccounts.get(i);
                        System.out.println(i+1 +". " + a.getAccountType() + " #" + a.getAccountNumber() + " balance: $" + String.format("%.2f", a.getBalance()));
                    }
                    try {
                        input = sc.nextLine();
                        n = Integer.parseInt(input);
                        Account acct = listOfAccounts.get(n-1);
                        System.out.println("Account #" + acct.getAccountNumber() + "'s transaction history");
                        List<Transaction> listOfTxns = txnDAO.getAllTransactionsAccount(acct);
                        for (int i = 0; i < listOfTxns.size(); i++) {
                            txn = listOfTxns.get(i);
                            int f = txn.getFrom();
                            int t = txn.getTo();
                            if (f > 0 && t == 0) {
                                System.out.println(i+1 + ". " + txn.getTransactionType() + " of $" + String.format("%.2f", txn.getAmount()) + " from account #" + f);
                            }
                            if (f == 0 && t > 0) {
                                System.out.println(i+1 + ". " + txn.getTransactionType() + " of $" + String.format("%.2f", txn.getAmount()) + " into account #" + t);
                            }
                            if (f > 0 && t > 0) {
                                System.out.println(i+1 + ". " + txn.getTransactionType() + " of $" + String.format("%.2f", txn.getAmount()) + " from account #" + f + " to account #" + t);
                            }
                        }
                        break;

                    } catch (NumberFormatException e) {
                        System.out.println("***INVALID INPUT***.......................................................");
                    } catch (IndexOutOfBoundsException e) {
                        System.out.println("***INVALID INPUT***.......................................................");
                    }
                }
            } else if (input.matches("6")) { // exit
//                for (int i = 0; i < 1000; i++) {
//                    System.out.println("");
//                }
                System.exit(0);
            } else if (input.matches("7") && customer.getAccounts().size() > 1) {
                // transfer from one account to another
                listOfAccounts = customer.getAccounts();
                Account acctFrom;
                int n;
                while (true) {
                    System.out.println("Select account to transfer from...........................................");
                    System.out.println("..........................................................................");
                    System.out.println("Enter a number to select an option........................................");
                    for (int i = 0; i < listOfAccounts.size(); i++) {
                        Account a = listOfAccounts.get(i);
                        System.out.println(i+1 +". " + a.getAccountType() + " #" + a.getAccountNumber() + " balance: $" + String.format("%.2f", a.getBalance()));
                    }
                    try {
                        input = sc.nextLine();
                        n = Integer.parseInt(input);
                        acctFrom = listOfAccounts.get(n-1);
                        break;
                    } catch (NumberFormatException e) {
                        System.out.println("***INVALID INPUT***.......................................................");
                    } catch (IndexOutOfBoundsException e) {
                        System.out.println("***INVALID INPUT***.......................................................");
                    }
                }
                while (true) {
                    System.out.println("Select account to transfer to.............................................");
                    System.out.println("..........................................................................");
                    System.out.println("Enter a number to select an option........................................");
                    for (int i = 0; i < listOfAccounts.size(); i++) {
                        Account a = listOfAccounts.get(i);
                        System.out.println(i+1 +". " + a.getAccountType() + " #" + a.getAccountNumber() + " balance: $" + String.format("%.2f", a.getBalance()));
                    }
                    try {
                        input = sc.nextLine();
                        n = Integer.parseInt(input);
                        Account acctTo = listOfAccounts.get(n-1);
                        System.out.println("transferring from accnt #" + acctFrom.getAccountNumber() + " to accnt #" + acctTo.getAccountNumber());
                        double amt = ui.transfer(acctFrom, acctTo);
                        Transaction txn = new Transaction(TransactionType.TRANSFER, amt);
                        txn.setTo(acctTo.getAccountNumber()); // this is a withdraw so no to account
                        txn.setFrom(acctFrom.getAccountNumber()); // from account

                        txnDAO.createTransaction(txn); // add txn to db
                        txn = txnDAO.getLastTransaction(); // identify just created record with this txn instance

                        // transfer funds from from account's balance to to account's balance
                        acctDAO.transfer(acctFrom, acctTo, txn);
                        acctFrom.setBalance(acctFrom.getBalance() - amt);
                        acctTo.setBalance(acctTo.getBalance() + amt);
                        System.out.println("Successful transfer!......................................................");
                        System.out.println(acctFrom.getAccountType() + " #" + acctFrom.getAccountNumber() + " new balance: $" + String.format("%.2f", acctFrom.getBalance()));
                        System.out.println(acctTo.getAccountType() + " #" + acctTo.getAccountNumber() + " new balance: $" + String.format("%.2f", acctTo.getBalance()));
                        Thread.sleep(2000);
                        break;

                    } catch (NumberFormatException e) {
                        System.out.println("***INVALID INPUT***.......................................................");
                    } catch (IndexOutOfBoundsException e) {
                        System.out.println("***INVALID INPUT***.......................................................");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            input = ui.mainMenu(customer);
        }
    }

    public static void openAccount() {
        Account newAccount = ui.openAccount(customer);
        // generate UNIQUE account number
        int num = newAccount.generateAccountNumber();
        while (accounts.containsKey(num)) {
            num = newAccount.generateAccountNumber();
        }
        // add this account instance to the current customer instance
        customer.addAccount(newAccount);
        // set the account instance's account number
        newAccount.setAccountNumber(num);
        // add account to db
        acctDAO.openNewAccount(newAccount, customer);

        System.out.println("Please make an initial deposit............................................");
        double amt = ui.deposit(newAccount);
        txn = new Transaction(TransactionType.DEPOSIT, amt);
        txn.setFrom(0); // this is a deposit so no from account
        txn.setTo(newAccount.getAccountNumber()); // to account

        txnDAO.createTransaction(txn); // add txn to db
        txn = txnDAO.getLastTransaction(); // identify just created record with this txn instance

        //add funds to to account's balance
        acctDAO.deposit(newAccount, txn);
        newAccount.setBalance(amt);
        System.out.println("Successful deposit!.......................................................");
        System.out.println(newAccount.getAccountType() + " #" + newAccount.getAccountNumber() + " new balance: $" + String.format("%.2f", newAccount.getBalance()));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        System.out.println("Welcome to Java Bank of the World!........................................");
        run();
    }
}
