package dev.parker.models;

import java.util.Objects;

public class Transaction {

    private int transactionNumber;
    private TransactionType transactionType;
    private int from;
    private int to;
    private double amount;

    public Transaction () {
        super();
    }



    public Transaction(TransactionType transactionType, double amount) {
        this.transactionType = transactionType;
        this.amount = amount;
    }

    public Transaction(int txnNumber, TransactionType transactionType, double amount) {
        this.transactionNumber = txnNumber;
        this.transactionType = transactionType;
        this.amount = amount;
    }

    public Transaction(int txnNumber, TransactionType transactionType, int from, int to, double amount) {
        this.transactionNumber = txnNumber;
        this.transactionType = transactionType;
        this.from = from;
        this.to = to;
        this.amount = amount;
    }

    public void setTransactionNumber(int transactionNumber) {
        this.transactionNumber = transactionNumber;
    }

    public TransactionType getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(TransactionType transactionType) {
        this.transactionType = transactionType;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return transactionNumber == that.transactionNumber && Double.compare(that.amount, amount) == 0 && transactionType == that.transactionType && Objects.equals(from, that.from) && Objects.equals(to, that.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionNumber, transactionType, from, to, amount);
    }

    @Override
    public String toString() {
        return "transactionNumber=" + transactionNumber +
                ", transactionType=" + transactionType +
                ", from=" + from +
                ", to=" + to +
                ", amount=" + String.format("%.2f", amount) +
                '}';
    }
}
