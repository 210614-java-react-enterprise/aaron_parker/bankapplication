package dev.parker.models;

public enum TransactionType {
    WITHDRAWAL, DEPOSIT, TRANSFER, DIRECT
}
