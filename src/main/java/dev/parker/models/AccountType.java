package dev.parker.models;

public enum AccountType {
    CHECKING, SAVINGS
}
