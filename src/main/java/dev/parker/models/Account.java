package dev.parker.models;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

public class Account {

    AccountType accountType;
    int accountNumber;
    double balance;
    int ownerId;
    ArrayList<String> transactions = new ArrayList<String>();

    public Account () {
        super();
    }

    public Account(int cust, AccountType accountType) {

        ownerId = cust;
        balance = 0;
        this.accountType = accountType;
        accountNumber = generateAccountNumber();
    }

    public Account(int ownerId, int accountNumber, double balance, AccountType accountType) {

        this.ownerId = ownerId;
        this.balance = balance;
        this.accountType = accountType;
        this.accountNumber = accountNumber;
    }

    public int generateAccountNumber() {
        int min = 100000000;
        int max = 999999999;
        Random rand = new Random();
        int num = rand.nextInt(max - min) + min;
        return num;
    }

    // returns the balance of the account
    public double getBalance() {

        return balance;
    }

    // returns the transaction history as an ArrayList of Strings
    public ArrayList<String> getTransactionHistory() {

        return transactions;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public ArrayList<String> getTransactions() {
        return transactions;
    }

    public void setTransactions(ArrayList<String> transactions) {
        this.transactions = transactions;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return accountNumber == account.accountNumber && Double.compare(account.balance, balance) == 0 && Objects.equals(accountType, account.accountType) && Objects.equals(transactions, account.transactions) && Objects.equals(ownerId, account.ownerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountType, accountNumber, balance, transactions, ownerId);
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountType='" + accountType + '\'' +
                ", accountNumber=" + accountNumber +
                ", balance=" + String.format("%.2f", balance) +
                ", transactions=" + transactions +
                ", owner=" + ownerId +
                '}';
    }
}
