package dev.parker.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Customer {

    private int id;
    private String username;
    private String password;
    private List<Account> accounts = new ArrayList<>();

    public Customer() {

        super();
        username = "";
        password = "";
    }

    public Customer(String username) {

        super();
        this.username = username;
    }

    public Customer(String username, String password) {

        super();
        this.username = username;
        this.password = password;
    }

    public Customer(String username, String password, int id) {

        super();
        this.username = username;
        this.password = password;
        this.id = id;
        accounts = new ArrayList<Account>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(ArrayList<Account> accounts) {
        this.accounts = accounts;
    }

    public void addAccount(Account account) {
        accounts.add(account);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return id == customer.id && Objects.equals(username, customer.username) && Objects.equals(password, customer.password) && Objects.equals(accounts, customer.accounts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, password, accounts);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", accounts=" + accounts +
                '}';
    }
}
