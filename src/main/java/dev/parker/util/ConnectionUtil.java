package dev.parker.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {

    private static Connection connection;

    public static Connection getConnection() throws SQLException {

        if(connection==null || connection.isClosed()) {
            String url = "jdbc:postgresql://training-db.cqnsz9u6mm7d.us-east-2.rds.amazonaws" +
                    ".com:5432/postgres?currentSchema=project-0";
            final String PASSWORD = System.getenv("PASSWORD");
            final String USERNAME = System.getenv("USERNAME");
            connection = DriverManager.getConnection(url, USERNAME, PASSWORD);
        }
        return connection;
    }
}
