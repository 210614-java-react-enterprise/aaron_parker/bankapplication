package dev.parker.ui;

import dev.parker.Driver;
import dev.parker.models.Account;
import dev.parker.models.AccountType;
import dev.parker.models.Customer;
import dev.parker.models.Transaction;

import java.util.InputMismatchException;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class BankAppUserInterface implements UserInterface {

    public String input;
    public Scanner sc = new Scanner(System.in);
    public boolean validInput;
    public boolean validUsername;
    public boolean validPassword;

    @Override
    public String welcome() {
        System.out.println("Please enter your username to sign in!....................................\n" +
                           "or press 1 to create an account...........................................");
        input = sc.nextLine();
        input = input.toLowerCase();
        validInput = this.validateInput(input);
        while (!validInput) {
            System.out.println("Sorry! Your input was invalid............................................");
            System.out.println("Please enter your username to sign in!...................................\n" +
                               "or press 1 to create an account...........................................");
            input = sc.nextLine();
            input = input.toLowerCase();
            validInput = validateInput(input);
        }
        return input;
    }

    @Override
    public String newUserWelcome() {

        System.out.println("Please create a username: (must be 4 - 16 characters......................\n" +
                           "long and begin with a letter).............................................");
        input = sc.nextLine();
        input = input.toLowerCase();
        validUsername = validateUsername(input);
        while (!validUsername) {
            // need to check here whether username is taken
            System.out.println("Sorry! that's not a valid username. Please choose another.................");
            input = sc.nextLine();
            input = input.toLowerCase();
            validUsername = validateUsername(input);
        }
        return input;
    }

    @Override
    public String createPassword() {

        System.out.println("Please create a password: (must be at least 8 characters..................\n" +
                           "and contain at least one digit, one uppercase letter......................\n" +
                           "and one special character)................................................");
        input = sc.nextLine();
        validPassword = validatePassword(input);
        while (!validPassword) {
            System.out.println("Sorry! that's not a valid password. Please choose another.................");
            input = sc.nextLine();
            validPassword = validatePassword(input);
        }

        // THIS IS THE RE_TYPE PASSWORD CODE UNCOMMENT THIS BEFORE FINAL COMMIT *******************

        String retypePassword = "";
        System.out.println("Please re-enter your password.............................................");
        retypePassword = sc.nextLine();

        while (!retypePassword.matches(input)) {
            System.out.println("Sorry! The passwords did not match!.......................................");
            System.out.println("Please re-enter your password.............................................");
            retypePassword = sc.nextLine();
        }

        System.out.println("Thank you! Your password is: " + input);
        System.out.println("..........................................................................");
        return input;
    }

    @Override
    public String enterPassword() {

        System.out.println("Please enter your password................................................");
        input = sc.nextLine();

        return input;
    }

    @Override
    public String mainMenu(Customer customer) {
        System.out.println("..........................................................................");
        System.out.println("Enter a number to select an option........................................");
        System.out.println("1. View Accounts..........................................................");
        System.out.println("2. Deposit................................................................");
        System.out.println("3. Withdraw...............................................................");
        System.out.println("4. Open New Account.......................................................");
        System.out.println("5. Statements.............................................................");
        System.out.println("6. Exit...................................................................");
        if (customer.getAccounts().size() > 1) {
            System.out.println("7. Transfer...............................................................");
        }
        input = sc.nextLine();
        while (!input.matches("[1234567]")) {
            System.out.println("..........................................................................");
            System.out.println("Enter a number to select an option........................................");
            System.out.println("1. View Accounts..........................................................");
            System.out.println("2. Deposit................................................................");
            System.out.println("3. Withdraw...............................................................");
            System.out.println("4. Open New Account.......................................................");
            System.out.println("5. Statements.............................................................");
            System.out.println("6. Exit...................................................................");
            if (customer.getAccounts().size() > 1) {
                System.out.println("7. Transfer...............................................................");
            }
            input = sc.nextLine();
        }
        return input;
    }

    @Override
    public String newUserMainMenu(Customer customer) {

        System.out.println("Would you like to open an account?........................................");
        System.out.println("..........................................................................");
        System.out.println("Enter a number to select an option........................................");
        System.out.println("1. Yes let's open an account..............................................");
        System.out.println("2. No...............................................................(Exit)");
        input = sc.nextLine();

        return input;
    }

    @Override
    public Account openAccount(Customer customer) {
        Account account = new Account();
        int acctNum = 0;
        int ownerId = 0;
        AccountType type = null;

        System.out.println("What kind of account would you like to open?..............................");
        System.out.println("..........................................................................");
        System.out.println("Enter a number to select an option........................................");
        System.out.println("1. Checking Account.......................................................");
        System.out.println("2. Savings Account........................................................");
        input = sc.nextLine();

        while (!input.matches("1") && !input.matches("2")) {
            System.out.println("****INVALID INPUT****.....................................................");
            System.out.println("What kind of account would you like to open?..............................");
            System.out.println("..........................................................................");
            System.out.println("Enter a number to select an option........................................");
            System.out.println("1. Checking Account.......................................................");
            System.out.println("2. Savings Account........................................................");
            input = sc.nextLine();
        }
        if (input.matches("1")) {
            ownerId = customer.getId();
//            acctNum = account.generateAccountNumber();
            type = AccountType.CHECKING;
        } else if (input.matches("2")) {
            ownerId = customer.getId();
//            acctNum = account.generateAccountNumber();
            type = AccountType.SAVINGS;
        }

//        account.setAccountNumber(acctNum);
        account.setAccountType(type);
        account.setBalance(0.0);
        account.setOwnerId(ownerId);

        System.out.println("Great! You've opened a " + type + " account!");
        System.out.println("..........................................................................");

        return account;
    }

    @Override
    public double deposit(Account account) {
        Transaction txn = new Transaction();
        boolean validDeposit = false;
        double amt = 0;
        while (!validDeposit) {
            while (true) {
                System.out.println("Enter an amount to deposit into Account #" + account.getAccountNumber());
                try {
                    input = sc.nextLine();
                    if (input.charAt(0) == '$') {
                        String str = input.substring(1);
                        amt = Double.parseDouble(str);
                    } else {
                        amt = Double.parseDouble(input);
                    }
                    break;
                } catch (NumberFormatException e) {
                    System.out.println("***INVALID INPUT***.......................................................");
                }
            }
            validDeposit = validateDeposit(amt);
        }
        return amt;
    }

    @Override
    public boolean validateDeposit(double input) {
        if (input > 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public double withdraw(Account account) {
        Transaction txn = new Transaction();
        boolean validWithdrawal = false;
        double amt = 0;
        while (!validWithdrawal) {
            while (true) {
                System.out.println("Enter an amount to withdraw from Account #" + account.getAccountNumber());
                try {
                    input = sc.nextLine();
                    if (input.charAt(0) == '$') {
                        String str = input.substring(1);
                        amt = Double.parseDouble(str);
                    } else {
                        amt = Double.parseDouble(input);
                    }
                    break;
                } catch (NumberFormatException e) {
                    System.out.println("***INVALID INPUT***.......................................................");
                }
            }
            validWithdrawal = validateWithdrawal(account.getBalance(), amt);
        }
        return amt;
    }

    @Override
    public boolean validateWithdrawal(double balance, double amt) {
        if (amt < 0) {
            return false;
        } else if (balance - amt < 0) {
            System.out.println("Insufficient Funds........................................................");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public double transfer(Account from, Account to) {
        boolean validTransfer = false;
        double amt = 0;
        while (!validTransfer) {
            while (true) {
                System.out.println("Enter an amount to transfer from Account #" + from.getAccountNumber() + " to Account # " + to.getAccountNumber());
                try {
                    input = sc.nextLine();
                    if (input.charAt(0) == '$') {
                        String str = input.substring(1);
                        amt = Double.parseDouble(str);
                    } else {
                        amt = Double.parseDouble(input);
                    }
                    break;
                } catch (NumberFormatException e) {
                    System.out.println("***INVALID INPUT***.......................................................");
                }
            }
            validTransfer = validateTransfer(from.getBalance(), amt);
        }
        return amt;
    }

    @Override
    public boolean validateTransfer(double balance, double amt) {
        if (amt < 0) {
            return false;
        } else if (balance - amt < 0) {
            System.out.println("Insufficient Funds........................................................");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean validateInput(String input) {

        if (input.matches("1")) { // check if new user
            return true;
        } else if (input.length() < 4 || input.length() > 16) { // validate length
            System.out.println("Username must be 4 - 16 characters long...................................");
            return false;
        } else if (!Character.toString(input.charAt(0)).matches("[a-zA-Z]")) { // validate first letter is a letter
            System.out.println("Username must begin with a letter.........................................");
            return false;
        } else if (!input.matches("\\w+")) { // validate all characters are alpha-numeric or underscore
            System.out.println("Username may only include alpha-numeric values and underscores............");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean validateUsername(String input) {

        if (input.length() < 4 || input.length() > 16) { // validate length
            System.out.println("Username must be 4 - 16 characters long...................................");
            return false;
        } else if (!Character.toString(input.charAt(0)).matches("[a-zA-Z]")) { // validate first letter is a letter
            System.out.println("Username must begin with a letter.........................................");
            return false;
        } else if (!input.matches("\\w+")) { // validate all characters are alpha-numeric or underscore
            System.out.println("Username may only include alpha-numeric values and underscores............");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean validatePassword(String input) {
        int nums = 0;
        int spChars = 0;
        int uppercases = 0;
        String str;
        String specials = "[\\.\\+\\*\\?\\^\\$\\(\\)\\[\\]\\{\\}\\|\\!@#$%&-_=;\\'\",]";
        if (input.length() >= 8) {
            for (int i = 0; i < input.length(); i++) {
                str = Character.toString(input.charAt(i));

                if(str.matches(specials)) {
                    spChars++;
                }

                if (Character.isDigit(input.charAt(i))) {
                    nums++;
                }

                if (Character.isUpperCase(input.charAt(i))) {
                    uppercases++;
                }
            }
            if (spChars < 1) {
                System.out.println("Password must contain at least 1 special character........................");
                return false;
            } else if (nums < 1) {
                System.out.println("Password must contain at least 1 number...................................");
                System.out.println("..........................................................................");
                System.out.println("Enter a number to select an option........................................");
                return false;
            } else if (uppercases < 1) {
                System.out.println("Password must contain at least 1 uppercase letter.........................");
                return false;
            } else {
                return true;
            }
        } else {
            System.out.println("Password must be at least 8 characters........................................");
            return false;
        }
    }
}
