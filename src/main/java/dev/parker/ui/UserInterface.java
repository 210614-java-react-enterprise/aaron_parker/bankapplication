package dev.parker.ui;

import dev.parker.models.Account;
import dev.parker.models.Customer;

public interface UserInterface {
    String welcome();
    String newUserWelcome();
    String createPassword();
    String enterPassword();
    String mainMenu(Customer customer);
    String newUserMainMenu(Customer customer);
    Account openAccount(Customer customer);

    double deposit(Account account);
    double withdraw(Account account);
    double transfer(Account to, Account from);

    boolean validateDeposit(double input);
    boolean validateWithdrawal(double input, double input1);
    boolean validateTransfer(double input, double input1);

    boolean validateInput(String input);
    boolean validateUsername(String input);
    boolean validatePassword(String input);
}
