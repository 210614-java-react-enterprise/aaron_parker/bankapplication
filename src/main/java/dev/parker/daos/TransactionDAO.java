package dev.parker.daos;

import dev.parker.models.Account;
import dev.parker.models.Transaction;

import java.util.List;
import java.util.Map;

public interface TransactionDAO {

    Map<Integer, Transaction> getAllTransactionsADMIN();
    List<Transaction> getAllTransactionsAccount(Account account);

    boolean createTransaction(Transaction transaction);

    Transaction getLastTransaction();
}
