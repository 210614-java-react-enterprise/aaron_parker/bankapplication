package dev.parker.daos;

import dev.parker.models.Customer;

import java.util.List;
import java.util.Map;

public interface CustomerDAO {


    Map<String, Customer> getAllCustomersByUsername();

    Map<Integer, Customer> getAllCustomersById();

    Customer getCustomer(String username);

    boolean createNewCustomer(Customer customer);
    boolean removeCustomer(Customer customer);
}

