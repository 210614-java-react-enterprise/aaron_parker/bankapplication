package dev.parker.daos;

import dev.parker.models.*;
import dev.parker.util.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransactionDAOImpl implements TransactionDAO {
    @Override
    public Map<Integer, Transaction> getAllTransactionsADMIN() {
        String sql = "select * from txn";
        try (Connection connection = ConnectionUtil.getConnection();
             Statement s = connection.createStatement();
             ResultSet rs = s.executeQuery(sql);) {
            Map<Integer, Transaction> allTxnsADMIN = new HashMap<Integer, Transaction>();
            while (rs.next()) {
                TransactionType txnType = TransactionType.valueOf(rs.getString("txn_type"));
                int txnNumber = rs.getInt("txn_num");
                double amt = rs.getDouble("amount");
                int txnNum = rs.getInt("txn_num");
                int fromAccnt = rs.getInt("from_accnt");
                int toAccnt = rs.getInt("to_accnt");

                Transaction transaction = new Transaction();
                transaction.setTransactionNumber(txnNumber);
                transaction.setTransactionType(txnType);
                transaction.setAmount(amt);
                transaction.setFrom(fromAccnt);
                transaction.setTo(toAccnt);

                allTxnsADMIN.put(txnNum, transaction);
            }
            return allTxnsADMIN;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Transaction> getAllTransactionsAccount(Account account) {
        String sql = "select * from txn where from_accnt=? or to_accnt=?";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, account.getAccountNumber());
            ps.setInt(2, account.getAccountNumber());
            ResultSet rs = ps.executeQuery();
            List<Transaction> allAcctTxns = new ArrayList<>();
            while (rs.next()) {
                TransactionType txnType = TransactionType.valueOf(rs.getString("txn_type"));
                int txnNumber = rs.getInt("txn_num");
                double amt = rs.getDouble("amount");
                int txnNum = rs.getInt("txn_num");
                int fromAccnt = rs.getInt("from_accnt");
                int toAccnt = rs.getInt("to_accnt");

                Transaction transaction = new Transaction();
                transaction.setTransactionNumber(txnNumber);
                transaction.setTransactionType(txnType);
                transaction.setAmount(amt);
                transaction.setFrom(fromAccnt);
                transaction.setTo(toAccnt);

                allAcctTxns.add(transaction);
            }
            return allAcctTxns;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean createTransaction(Transaction transaction) {
        String sql = "insert into txn values (default, ?::txntype, ?, ?, ?)";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, transaction.getTransactionType().name());
            ps.setInt(2, transaction.getFrom());
            ps.setInt(3, transaction.getTo());
            ps.setDouble(4, transaction.getAmount());
            int success = ps.executeUpdate();
            if (success > 0) {
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public Transaction getLastTransaction() {
        Transaction transaction = new Transaction();
        String sql = "select * from txn where txn_num=(select max(txn_num) from txn)";
        try (Connection connection = ConnectionUtil.getConnection();
             Statement s = connection.createStatement();
             ResultSet rs = s.executeQuery(sql);) {
            while (rs.next()) {
                TransactionType txnType = TransactionType.valueOf(rs.getString("txn_type"));
                int txnNumber = rs.getInt("txn_num");
                double amt = rs.getDouble("amount");
                int txnNum = rs.getInt("txn_num");
                int fromAccnt = rs.getInt("from_accnt");
                int toAccnt = rs.getInt("to_accnt");

                transaction.setTransactionNumber(txnNumber);
                transaction.setTransactionType(txnType);
                transaction.setAmount(amt);
                transaction.setFrom(fromAccnt);
                transaction.setTo(toAccnt);
            }
            return transaction;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }
}
