package dev.parker.daos;

import dev.parker.models.Account;
import dev.parker.models.Customer;
import dev.parker.util.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomerDAOImpl implements CustomerDAO {
//    @Override
//    public List<Customer> getAllCustomers() {
//        String sql = "select id, username, pass from customer";
//        try (Connection connection = ConnectionUtil.getConnection();
//             Statement s = connection.createStatement();
//             ResultSet rs = s.executeQuery(sql);) {
//            List<Customer> allCustomers = new ArrayList<Customer>();
//            while (rs.next()) {
//                String custUsername = rs.getString("username");
//                String custPassword = rs.getString("pass");
//
//                Customer customer = new Customer();
//                customer.setUsername(custUsername);
//                customer.setPassword(custPassword);
//                customer.setId(rs.getInt("id"));
//                allCustomers.add(customer);
//            }
//            return allCustomers;
//        } catch (SQLException throwables) {
//            throwables.printStackTrace();
//        }
//        return null;
//    }

    @Override
    public Map<String, Customer> getAllCustomersByUsername() {
        String sql = "select id, username, pass from customer";
        try (Connection connection = ConnectionUtil.getConnection();
             Statement s = connection.createStatement();
             ResultSet rs = s.executeQuery(sql);) {
            Map<String, Customer> allCustomers = new HashMap<String, Customer>();
            while (rs.next()) {
                String custUsername = rs.getString("username");
                custUsername = custUsername.toLowerCase();
                String custPassword = rs.getString("pass");
                int custId = rs.getInt("id");

                Customer customer = new Customer(custUsername, custPassword, custId);
                allCustomers.put(custUsername, customer);
            }
            return allCustomers;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Map<Integer, Customer> getAllCustomersById() {
        String sql = "select id, username, pass from customer";
        try (Connection connection = ConnectionUtil.getConnection();
             Statement s = connection.createStatement();
             ResultSet rs = s.executeQuery(sql);) {
            Map<Integer, Customer> allCustomers = new HashMap<Integer, Customer>();
            while (rs.next()) {
                String custUsername = rs.getString("username");
                custUsername = custUsername.toLowerCase();
                String custPassword = rs.getString("pass");
                int custId = rs.getInt("id");

                Customer customer = new Customer(custUsername, custPassword, custId);
                allCustomers.put(custId, customer);
            }
            return allCustomers;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Customer getCustomer(String username) {
        Customer customer;
        String sql = "select * from customer where username=?";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);
             ) {
            ps.setString(1,username);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String pass = rs.getString("pass");
                int id = rs.getInt("id");
                customer = new Customer(username, pass, id);
                return customer;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean createNewCustomer(Customer customer) {
        String sql = "insert into customer values (default, ?,?)";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, customer.getUsername());
            ps.setString(2, customer.getPassword());
            int success = ps.executeUpdate();
            if (success > 0) {
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean removeCustomer(Customer customer) {
        String sql = "delete from customer where id=?";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql);) {

            ps.setInt(1, customer.getId());
            int success = ps.executeUpdate();
            if (success > 0) {
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }
}