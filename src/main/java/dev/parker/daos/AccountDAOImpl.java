package dev.parker.daos;

import dev.parker.models.Account;
import dev.parker.models.AccountType;
import dev.parker.models.Customer;
import dev.parker.models.Transaction;
import dev.parker.util.ConnectionUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AccountDAOImpl implements AccountDAO {
    @Override
    public Map<Integer, Account> getAllAccountsADMIN() {
        String sql = "select accnt_num, accnt_type, balance, cust_id from account";
        try (Connection connection = ConnectionUtil.getConnection();
             Statement s = connection.createStatement();
             ResultSet rs = s.executeQuery(sql);) {
            Map<Integer, Account> allAccountsADMIN = new HashMap<Integer, Account>();
            while (rs.next()) {
                AccountType accntType = AccountType.valueOf(rs.getString("accnt_type"));
                double bal = rs.getDouble("balance");
                int accntNum = rs.getInt("accnt_num");
                int owner = rs.getInt("cust_id");

                Account account = new Account(owner, accntNum, bal, accntType);
                allAccountsADMIN.put(accntNum, account);
            }
            return allAccountsADMIN;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Account> getAllAccounts(Customer customer) {
        return null;
    }

    @Override
    public boolean openNewAccount(Account newAccount, Customer customer) {
        String sql = "insert into account values (?, ?::accnttype, ?, ?)";
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, newAccount.getAccountNumber());
            ps.setString(2, newAccount.getAccountType().name());
            ps.setDouble(3, newAccount.getBalance());
            ps.setInt(4, customer.getId());
            int success = ps.executeUpdate();
            if (success > 0) {
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean closeAccount(Account newAccount) {
        return false;
    }

    @Override
    public boolean deposit(Account acct, Transaction txn) {
        String sql = "update account set balance = ? where accnt_num = ?";
        int acctNumber = acct.getAccountNumber();
        double txnAmount = txn.getAmount();
        double acctBalance = acct.getBalance();
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setDouble(1, acctBalance + txnAmount);
            ps.setInt(2, acctNumber);
            int success = ps.executeUpdate();
            if (success > 0) {
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean withdraw(Account acct, Transaction txn) {
        String sql = "update account set balance = ? where accnt_num = ?";
        int acctNumber = acct.getAccountNumber();
        double txnAmount = txn.getAmount();
        double acctBalance = acct.getBalance();
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setDouble(1, acctBalance - txnAmount);
            ps.setInt(2, acctNumber);
            int success = ps.executeUpdate();
            if (success > 0) {
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean transfer(Account from, Account to, Transaction txn) {
        String sql = "update account set balance = ? where accnt_num = ?; update account set balance = ? where accnt_num = ?;";
        int acctFrom = from.getAccountNumber();
        int acctTo = to.getAccountNumber();
        double txnAmount = txn.getAmount();
        double fromBalance = from.getBalance();
        double toBalance = to.getBalance();
        try (Connection connection = ConnectionUtil.getConnection();
             PreparedStatement ps = connection.prepareStatement(sql)){
            ps.setDouble(1, fromBalance - txnAmount);
            ps.setInt(2, acctFrom);
            ps.setDouble(3, toBalance + txnAmount);
            ps.setInt(4, acctTo);
            int success = ps.executeUpdate();
            if (success > 0) {
                return true;
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return false;
    }
}
