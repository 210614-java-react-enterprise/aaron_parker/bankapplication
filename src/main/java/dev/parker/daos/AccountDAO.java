package dev.parker.daos;

import dev.parker.models.Account;
import dev.parker.models.Customer;
import dev.parker.models.Transaction;

import java.util.List;
import java.util.Map;

public interface AccountDAO {

    Map<Integer, Account> getAllAccountsADMIN(); // returns information all all registered accounts (ADMIN only)
    List<Account> getAllAccounts(Customer customer); // returns list of All accounts attached to specified customer

    boolean openNewAccount(Account newAccount, Customer customer);

    boolean closeAccount(Account newAccount);
    boolean deposit(Account acct, Transaction txn);
    boolean withdraw(Account acct, Transaction txn);
    boolean transfer(Account from, Account to, Transaction txn);
}

